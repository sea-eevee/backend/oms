.PHONY : network-create db-reload app-build app-run app-stop app-clean

network:
	docker network inspect compfest-marketplace >/dev/null 2>&1 || \
        docker network create --driver bridge compfest-marketplace

db-reload:
	docker container stop compfest-marketplace-service-oms-db || true
	docker build -t compfest-marketplace-service-oms-db-img .
	docker run -d --rm --name compfest-marketplace-service-oms-db \
        --network=compfest-marketplace \
        -p 13400:5432 \
        -v compfest-marketplace-oms-vol:/var/lib/postgresql/data \
        -e POSTGRES_USER=pgku \
        -e POSTGRES_PASSWORD=pgku \
        -e POSTGRES_DB=oms \
        compfest-marketplace-service-oms-db-img