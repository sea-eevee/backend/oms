CREATE TABLE merchant_wallet (
  merchant_id BIGINT NOT NULL, --REFERENCES merchants (id),
  real_wallet_balance BIGINT NOT NULL DEFAULT 0
);

CREATE TABLE item_merchant_stock (
  item_id BIGINT PRIMARY KEY,
  merchant_id BIGINT NOT NULL, -- REFERENCES merchant (id),
  stock int NOT NULL DEFAULT 100
);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (1, 1);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (2, 1);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (3, 1);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (4, 1);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (5, 2);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (6, 2);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (7, 3);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (8, 3);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (9, 3);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (10, 4);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (11, 4);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (12, 4);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (13, 5);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (14, 5);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (15, 5);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (16, 6);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (17, 6);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (18, 6);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (19, 7);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (20, 8);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (21, 9);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (22, 10);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (23, 11);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (24, 12);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (25, 13);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (26, 14);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (27, 15);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (28, 16);
INSERT INTO item_merchant_stock(item_id, merchant_id) VALUES (29, 17);

CREATE TABLE orders (
  id BIGSERIAL PRIMARY KEY,
  customer_id BIGINT NOT NULL, -- REFERENCES customers (id), before insert, check first to customer service
  merchant_id BIGINT NOT NULL, -- REFERENCES merchants (id), before insert, check first to merchant service
  is_paid BOOLEAN DEFAULT FALSE,
  order_date BIGINT NOT NULL DEFAULT (EXTRACT(EPOCH FROM NOW()) * 1000)
);
INSERT INTO orders(id, customer_id, merchant_id, is_paid) VALUES (1, 1, 1, true);
INSERT INTO orders(id, customer_id, merchant_id, is_paid) VALUES (2, 2, 1, true);
INSERT INTO orders(id, customer_id, merchant_id, is_paid) VALUES (3, 3, 1, true);
select setval('orders_id_seq'::regclass,4,false);

CREATE TABLE order_detail (
  order_id BIGINT NOT NULL REFERENCES orders (id),
  item_id BIGINT NOT NULL, -- REFERENCES items (id), before insert, check first to merchant service
  quantity INT DEFAULT 1
);
INSERT INTO order_detail(order_id, item_id, quantity) VALUES (1, 1, 2);
INSERT INTO order_detail(order_id, item_id, quantity) VALUES (1, 2, 3);
INSERT INTO order_detail(order_id, item_id, quantity) VALUES (1, 3, 4);
INSERT INTO order_detail(order_id, item_id, quantity) VALUES (1, 4, 5);
INSERT INTO order_detail(order_id, item_id, quantity) VALUES (2, 5, 2);
INSERT INTO order_detail(order_id, item_id, quantity) VALUES (2, 6, 3);
INSERT INTO order_detail(order_id, item_id, quantity) VALUES (3, 7, 4);
INSERT INTO order_detail(order_id, item_id, quantity) VALUES (3, 8, 5);
INSERT INTO order_detail(order_id, item_id, quantity) VALUES (3, 9, 2);

CREATE TABLE transaction (
  order_id BIGINT NOT NULL UNIQUE REFERENCES orders (id),
  bank_name varchar(255) DEFAULT 'Lorem ipsum',
  bank_account_number varchar(255) DEFAULT '123456789',
  admin_approval BOOLEAN,
  transaction_date BIGINT NOT NULL DEFAULT (EXTRACT(EPOCH FROM NOW()) * 1000)
);
INSERT INTO transaction(order_id) VALUES (1);
INSERT INTO transaction(order_id) VALUES (2);
INSERT INTO transaction(order_id) VALUES (3);